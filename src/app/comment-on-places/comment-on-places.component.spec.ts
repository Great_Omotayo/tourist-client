import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentOnPlacesComponent } from './comment-on-places.component';

describe('CommentOnPlacesComponent', () => {
  let component: CommentOnPlacesComponent;
  let fixture: ComponentFixture<CommentOnPlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentOnPlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentOnPlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
