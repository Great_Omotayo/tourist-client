import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarForSmallDeviceComponent } from './sidebar-for-small-device.component';

describe('SidebarForSmallDeviceComponent', () => {
  let component: SidebarForSmallDeviceComponent;
  let fixture: ComponentFixture<SidebarForSmallDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarForSmallDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarForSmallDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
