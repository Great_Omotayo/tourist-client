import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../services/data.service';
declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-sidebar-for-small-device',
  templateUrl: './sidebar-for-small-device.component.html',
  styleUrls: ['./sidebar-for-small-device.component.css']
})
export class SidebarForSmallDeviceComponent implements OnInit {
max_price:number=0;
location:string;
wic:string;
filter:boolean = true;
attract:boolean=false;
state:boolean = false;
beach:boolean= false;
reserve:boolean= false;
mountain:boolean=false;
resort:boolean= false;
waterfall:boolean = false;
most_visit:boolean = false;
recommendation:boolean = false;
hot_deals:boolean = false;
  constructor(private router:Router,private dataService: DataService) { }

  ngOnInit() {
  	this.filter = true;
  }
  ngAfterViewInit() {
    $(document).ready(function(){

    });
  }
  searchByBudget(){
    let search = {location:'',type:'',max_price:this.max_price};
    this.dataService.updateSearch(search);
    this.filter = false;
    this.router.navigate(['/search']);
  }
  searchByLocation(){
    let search = {location:this.location,type:'',max_price:this.max_price};
    this.dataService.updateSearch(search);
    this.filter = false;
    this.router.navigate(['/search']);
  }
  attraction(type:any){
     if(type=='0' && this.beach == true){
       this.reserve = false;
       this.mountain = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Beach';
     }else if(type == '1' && this.reserve == true){
       this.beach = false;
       this.mountain = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Game Reserve';
     }else if(type == '2' && this.mountain == true){
       this.reserve = false;
       this.beach = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Mountains and Hills';
     }else if(type == '3' && this.resort == true){
       this.reserve = false;
       this.mountain = false;
       this.beach = false;
       this.waterfall = false;
       this.wic = 'Resort';
     }else if(type == '4' && this.waterfall == true){
       this.reserve = false;
       this.mountain = false;
       this.resort = false;
       this.beach = false;
       this.wic = 'Waterfall'
     }
     let search = {location:'',type:this.wic,max_price:this.max_price};
     this.dataService.updateSearch(search);
     this.filter = false;
      this.router.navigate(['/search']);

  }
  hotSearch(type:any){
  	if(type=='0' && this.recommendation == true){
       this.hot_deals = false;
       this.most_visit = false;
       this.wic = '';
     }else if(type == '1' && this.hot_deals == true){
       this.recommendation = false;
       this.most_visit = false;
       this.wic = '';
     }else if(type == '2' && this.most_visit == true){
       this.recommendation = false;
       this.hot_deals = false;
       this.wic = '';
     }
     let search = {location:'',type:this.wic,max_price:this.max_price};
     this.dataService.updateSearch(search);
     this.filter = false;
      this.router.navigate(['/search']);
  }


}
