import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselForSmallDevicesComponent } from './carousel-for-small-devices.component';

describe('CarouselForSmallDevicesComponent', () => {
  let component: CarouselForSmallDevicesComponent;
  let fixture: ComponentFixture<CarouselForSmallDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselForSmallDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselForSmallDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
