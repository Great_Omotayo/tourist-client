import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel-for-small-devices',
  templateUrl: './carousel-for-small-devices.component.html',
  styleUrls: ['./carousel-for-small-devices.component.css']
})
export class CarouselForSmallDevicesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
