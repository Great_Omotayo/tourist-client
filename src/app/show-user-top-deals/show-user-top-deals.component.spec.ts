import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUserTopDealsComponent } from './show-user-top-deals.component';

describe('ShowUserTopDealsComponent', () => {
  let component: ShowUserTopDealsComponent;
  let fixture: ComponentFixture<ShowUserTopDealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowUserTopDealsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUserTopDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
