import { Component, OnInit,ElementRef, Input } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { RouterModule } from '@angular/router';
//import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import { Attraction } from './attr';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

declare var $:any;
declare var jQuery:any;
const URL = 'http://localhost:8080/upload';

@Component({
  selector: 'app-attraction',
  templateUrl: './attraction.component.html',
  styleUrls: ['./attraction.component.css'],
})

export class AttractionComponent implements OnInit {
  formData:any = new FormData();
  attrname:string;
  adminpass:string;
  cpassword:string;
  attremail:string;	
  attrpNumber:number;
  location:string;
  town:string;
  drivingInstruction:string;
  category:string;
  adminName:string;
  adminEmail:string;
  adminpNumber:number;
  attraddress:string;
  facility:string;
  facilities:string[] = [];
  files:any= [];
  fileName = [];
  message:string;
  hideMsg:boolean= true;
  hideloading:boolean=true;
  photo:any = [];
  newattr:any;
  validateResult:string;
  priceperhead:number;
  what:string = 'Save';
  to:boolean = false;
  terms:boolean = false;
  constructor(private router:Router,
              private http:Http,
              private el: ElementRef,
              private validateService: ValidateService,
              private authService: AuthService,
              private dataService: DataService) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    $(document).ready(function(){
     $('#open').click(function(){
        let formData = new FormData();
         formData.append('okay','gare');  
         console.log(formData);
     });
    });
  }
  add(){
  	//alert(this.facility);
  	this.facilities.push(this.facility);
  	this.facility = '';
  }
  removefacility(facility){
    let index = this.facilities.indexOf(facility);
    this.facilities.splice(index,1);
  }
  removeimg(img){
    let index = this.photo.indexOf(img);
    this.photo.splice(index,1);
  }
  fileChange(){
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    let fileCount: number = inputEl.files.length;
    this.photo.push(inputEl.files.item(0).name);
    this.formData.append('photo', inputEl.files.item(0));
  }
  updatelocally(attr){
    this.dataService.updateAttr(attr);
        //console.log(this.newattr);
  }
  saveAttraction(){ 
       let attr = new Attraction();
       attr.AttractionName = this.attrname;
  	   attr.AttractionEmail = this.attremail;
  	   attr.AttractionPhone = this.attrpNumber;
  	   attr.AttractionLocation = this.location;
  	   attr.AttractionTown = this.town;
       attr.drivingInstruction = this.drivingInstruction;
  	   attr.AttractionCategory = this.category;
  	   attr.AttractionAddress = this.attraddress;
       attr.Priceperhead = this.priceperhead;
  	   attr.admin = {
  	   	username:this.adminName,
  	   	email: this.adminEmail,
  	   	phone:this.adminpNumber,
        password:this.adminpass 
  	   };
       if(this.facilities.length > 0){
        attr.Facilities.push.apply(attr.Facilities, this.facilities);
       }
      this.validateResult =  this.validateService.Attraction_details(attr,this.cpassword,this.photo);
      if(this.validateResult != '') {this.hideMsg = false; return;}
      if(this.terms == false) {
      this.hideMsg = false;  
      this.validateResult = 'Please agree with terms and Conditions'; return;
      } 
      this.formData.append('data',JSON.stringify(attr));
      //console.log(attr); 
      this.what = 'Loading....';
      this.to = true;
      //this.hideloading = false;
      this.authService.addAttr(this.formData).subscribe(data =>{
        //this.hideloading = true;
        if(data.status == 'success'){
           //this.updatelocally(data.res);
           this.clearform(attr,this.formData);
           this.router.navigate(['/msg']);
        }
        else{
           this.dataService.updatetoWhere('add_attraction');
           this.router.navigate(['/errormsg']);
        }   
      });

  }
   clearform(attr,formData){
       formData = [];
       this.attrname = '';
       this.attremail = '';
       this.attrpNumber = undefined;
       this.location='';
       this.town = '';
       this.drivingInstruction = '';
       this.category = '';
       this.attraddress = '';
       attr.admin = {};
       this.photo = [];
       this.facilities = [];
   }
  
}
