import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUserFrequentlyComponent } from './show-user-frequently.component';

describe('ShowUserFrequentlyComponent', () => {
  let component: ShowUserFrequentlyComponent;
  let fixture: ComponentFixture<ShowUserFrequentlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowUserFrequentlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUserFrequentlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
