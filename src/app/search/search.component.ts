import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  display: any = 'none';
  breadcrumb:any;
  attr:any;
  location:string = '';
  category:string = '';
  max_price:string = '0';
  hideRes:boolean = true;
  isempty:boolean = false;
  list:boolean = false;
  attrs:{};
  facilities:any;
  what:string='ok';
  constructor(private router:Router,
              private validateService: ValidateService,
              private authService: AuthService,
              private dataService: DataService) { }

  ngOnInit() {
    alert(window.innerWidth);
    this.dataService.currentsearch.subscribe(search=>{ 
    this.hideRes = false;
    this.list = true;
    this.authService.getAttr(search).subscribe(data=>{
    this.ngAfterViewInit();  
    this.hideRes = true;
    this.isempty = false;
    this.list = false;
     if(data.status == 'success'){
        this.attr = data.res;
        console.log(this.attr);
        this.dataService.updateAttr(data.res);
        if(this.attr.length == 0){this.isempty = true;}
     }else{}
    });
    });
   //  this.attrs = {AttractionLocation:'okay',AttractionCategory:'okayyes'};
   //  this.dataService.currentAttr.subscribe(data=>{
   //    this.isempty = false;
   //    this.attr = data;
   //    if(this.attr.length == undefined) this.router.navigate(['/']);
   //    if(this.attr.length == 0){
   //         this.isempty = true;
   //    }
   // });
    
  }
  search(){
    this.hideRes = false;
    this.isempty = false;
    this.list = true;
    let search = {location:this.location,type:this.category,max_price:parseInt(this.max_price)};
    this.authService.getAttr(search).subscribe(data=>{
     this.list = false; 
     if(data.status == 'success'){
       this.hideRes = true;
       this.attr = data.res;
       if(this.attr.length == 0){
         this.isempty = true;
       }
     }else{}
    });
    
  }
  gotoview(attr){
    this.dataService.viewAttraction([attr]);
    this.router.navigate(['/view']);
  }
  book(attr){
     this.dataService.updatetoBook([attr]);
     this.router.navigate(['/book']);
  }
  Showfilters(){
  	this.display = {display:'block'};
  	this.breadcrumb = {display:'none'};
  }
  Hidefilters(){
  	this.display = {display:'none'};
  	this.breadcrumb = {display:'block'};
  }
  ngAfterViewInit() {
    $(document).ready(function(){
      $('.overlay').addClass('hide');
      $('.filters').click(function(){
         $('.overlay').removeClass('hide');
      });
      $('.Hidefilters').click(function(){
        $('.overlay').addClass('hide');
      });
    });
  }
}
