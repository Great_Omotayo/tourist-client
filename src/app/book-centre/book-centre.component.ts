import { Component, OnInit } from '@angular/core';
//import { User } from './user';
import { Book } from './book';
import { Router } from '@angular/router';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-book-centre',
  templateUrl: './book-centre.component.html',
  styleUrls: ['./book-centre.component.css']
})
export class BookCentreComponent implements OnInit {
//name:string='';
hideloading:boolean = true;
phone:number;
email:string;
cfemail:string;
tour_date:string;
TotalPrice:number;
attr_id: string;
priceperhead:number;
tourist:number =1;
valRes:string;
attr:any;
image:string;
hidemessage:boolean = true;
hidesuccess:boolean = true;
address:string;
totalPrice:number;
successmsg:string;
to:boolean = false;
viatm:boolean = false;
atmbtn:boolean = false;
depbtn:boolean = false;
  constructor(private validatService:ValidateService,
  	          private authService: AuthService,
  	          private router: Router,
  	          private dataService: DataService) { }

  ngOnInit() {
  	  this.dataService.currenttoBook.subscribe(attr => {
  		this.attr = attr; 
      //console.log(this.attr);
      if(this.attr.length == undefined) this.router.navigate(['/']);
  		this.image = this.attr[0].Images[0];
      this.attr_id = this.attr[0]._id;
  		this.address = this.attr[0].AttractionAddress;
  		this.totalPrice = this.attr[0].Priceperhead;
  	  });
  	//this.dataService.viewAttr.subscribe(price => this.priceperhead = price);
    //this.dataService.currenttoBook.subscribe(attr=>th)
  }
  route(){
  	this.dataService.updatebankdet({
    			tourist:4,
    			amount:23000
    		});
    		this.router.navigate(['/bankdetails']);
  }
  onKey(value:number){
  	this.totalPrice = value * parseInt(this.attr[0].Priceperhead);;
  }
  bankdep(){

  	let book = new Book();
  	book.user = {name:'',phone:'',email:this.email};
  	book.attraction_id = this.attr_id;
  	book.amount_paid = 0;
  	book.tour_date = this.tour_date;
  	book.tourist = this.tourist;
  	book.modeOfPayment = 'Bank';
  	this.valRes = this.validatService.validateUserAndBook(book,this.cfemail);  	if(this.valRes != '') {this.hidemessage = false;return;}
  	this.to = true;
    this.atmbtn = true;
    this.authService.saveBook(book).subscribe(data=> {
    	if(data.status == 'success'){
        this.hideloading = true;
    		this.dataService.updatebankdet({
    			tourist:this.tourist,
    			amount:this.tourist * this.attr.Priceperhead
    		});
    		this.router.navigate(['/bankdetails']);
    	}else{
    		this.dataService.updatetoWhere('book');
    		this.router.navigate(['/errormsg']);
    	}
    })
  }
  viaAtm(){
  	let book = new Book();
  	book.user = {name:'',phone:'',email:this.email};
  	book.attraction_id = this.attr_id;
  	book.amount_paid = 0;
  	book.tour_date = this.tour_date;
  	book.tourist = this.tourist;
  	book.modeOfPayment = 'Paystack';
  	this.valRes = this.validatService.validateUserAndBook(book,this.cfemail);  	if(this.valRes != '') {this.hidemessage = false;return;}
  	console.log(book);
  	this.viatm = true;
    this.depbtn = true;
    this.authService.saveBook(book).subscribe(data=> {
    	if(data.status == 'success'){
        this.hideloading = true;
        this.hidesuccess = false;
        this.hidemessage = true;
        this.successmsg = 'Loading Paystack.....';
        this.clearform();
    		//load paystack
    		//book.user_id = data.res._id;
    	}else{
    		this.dataService.updatetoWhere('book');
    		this.router.navigate(['/errormsg']);
    	}
    });
  }
  clearform(){
    this.tourist = 1;
    this.tour_date = '';
    this.email = '';
    this.cfemail = '';
  }
}
