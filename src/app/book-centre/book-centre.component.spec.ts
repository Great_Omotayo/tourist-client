import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCentreComponent } from './book-centre.component';

describe('BookCentreComponent', () => {
  let component: BookCentreComponent;
  let fixture: ComponentFixture<BookCentreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookCentreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
