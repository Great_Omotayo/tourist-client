export class Book {
  attraction_id: string;
  amount_paid: number;
  tour_date: string;
  tourist:number;
  modeOfPayment:string;
  user:{};
  cancel:boolean = false;
}