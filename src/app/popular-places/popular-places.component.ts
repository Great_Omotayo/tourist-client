import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-popular-places',
  templateUrl: './popular-places.component.html',
  styleUrls: ['./popular-places.component.css']
})
export class PopularPlacesComponent implements OnInit {
which:string;
  constructor(private authService: AuthService,
  	          private dataService: DataService,private router:Router) { }

  ngOnInit() {
  }
  whichOne(tour:number){
  	if(tour == 0)
  		this.which = 'beach';
  	else if(tour == 1)
  		this.which = 'Resort';
  	else if(tour == 2)
  		this.which = 'Game reserve';
  	else if(tour == 3)
  		this.which = 'Mountain and Hill';
  	else if(tour == 4)
  		this.which = 'Waterfall';
  	else 
  		this.which = '';
  }
  type(tour:number){
  	this.whichOne(tour);
  	let search = {location:'',type:this.which,max_price:0};
    this.dataService.updateSearch(search);
    this.router.navigate(['/search']);
  }
}
