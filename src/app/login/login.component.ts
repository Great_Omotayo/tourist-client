import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  showmsg:string;
  password:string;
  alert:boolean=true;
  roller:boolean=true;
  disable:boolean= false;
  constructor(private router:Router,
  	          private dataService: DataService,
  	          private authService: AuthService,
  	          private validateService: ValidateService) { }

  ngOnInit() {
  }

  doLogin(){
  	let authdata = {email:this.email,password:this.password};
    this.roller = false;
    this.disable = true;
  	this.authService.authenticate(authdata).subscribe(data =>{
      if(data.status =='success' && data.message == 1){
      	this.authService.savetoLocalStorage(data.res.token,data.res.result.attraction);
        this.dataService.updateAdmin([data.res.result.attraction]);
        this.dataService.updateBooked(data.booked);
        console.log(data.res.result.attraction);
        console.log(data.booked);
        this.router.navigate(['/']);
         //console.log(data.res);
      }else if(data.status =='success' && data.message == 0){
      	this.alert = false;
        this.showmsg = 'Email and Password does not matched';
      }else if(data.status == 'failed' && data.message == 1){
      	this.alert = false;
      	this.showmsg = 'Account does not exist....';
      }else{
      	this.alert = false;
      	this.showmsg = 'Cant log in, please try again later';
      }
  	});
  }
}
