import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUploadImagesComponent } from './admin-upload-images.component';

describe('AdminUploadImagesComponent', () => {
  let component: AdminUploadImagesComponent;
  let fixture: ComponentFixture<AdminUploadImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUploadImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUploadImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
