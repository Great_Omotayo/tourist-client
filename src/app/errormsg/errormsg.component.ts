import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-errormsg',
  templateUrl: './errormsg.component.html',
  styleUrls: ['./errormsg.component.css']
})
export class ErrormsgComponent implements OnInit {
  towhere:string;
  constructor(private dataService:DataService) { }

  ngOnInit() {
  	this.dataService.currentToWhere.subscribe(location => {
  		this.towhere = location;
  	});
  }

}
