import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

declare var $:any;

@Component({
  selector: 'app-view-tabs',
  templateUrl: './view-tabs.component.html',
  styleUrls: ['./view-tabs.component.css']
})
export class ViewTabsComponent implements OnInit {
attraction:any;
say:string;
  constructor(private dataService: DataService,private authService:AuthService) { }

  ngOnInit() {
    this.dataService.viewAttr.subscribe(toview => this.attraction = toview);
    console.log(this.attraction);
  }
  enter(value:string){
    this.authService.updatecomment(value+':'+this.attraction[0]._id);
  }
  ngAfterViewInit() {
  	$(function(){
    $('a[title]').tooltip();
   });
  }

}
