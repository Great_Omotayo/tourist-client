import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  attr:any;
  isliked:boolean=false;
  like:number;
  Images=[];
  constructor(private authService: AuthService,
    private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.dataService.currentview.subscribe(toview =>{
      this.attr = toview;
      this.like = 0;
      if(this.attr.length ==undefined) this.router.navigate(['/']);
      this.like = this.attr[0].Likes;
      for(var u=0;u<this.attr[0].Images.length;u++){
        if(u==4) return;
        this.Images.push(this.attr[0].Images[u]);
      }
    });
    //this.Images=['1.jpg','2.jpg','3.jpg','4.jpg'];
    //console.log(this.Images);
  }
  likes(){
  	if(this.isliked == true){
      this.isliked = false;	
      this.like = this.like - 1;
      this.authService.updatelike(this.like);
    }
    else{
      this.isliked = true;	
      this.like = this.like + 1;	
      this.authService.updatelike(this.like);
    }
  }
  book(attr){
    this.dataService.updatetoBook([attr]);
    this.router.navigate(['/book']);
  }
  ngAfterViewInit() {
    (function( $ ) {

      //Function to animate slider captions 
      function doAnimations( elems ) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        
        elems.each(function () {
          var $this = $(this),
          $animationType = $this.data('animation');
          $this.addClass($animationType).one(animEndEv, function () {
            $this.removeClass($animationType);
          });
        });
      }
      
      //Variables on page load 
      var $myCarousel = $('#carousel-example-generic'),
      $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
      
      //Initialize carousel 
      $myCarousel.carousel();
      
      //Animate captions in first slide on page load 
      doAnimations($firstAnimatingElems);
      
      //Pause carousel  
      $myCarousel.carousel('pause');
      
      
      //Other slides to be animated on carousel slide event 
      $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
      });  
      $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
      });
      
    })(jQuery);	

  }
}
