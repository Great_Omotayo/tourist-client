import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  hide_booked: boolean = false;
  hide_cancel = true;
  hide_toured:boolean = true;
  c:number[] = [1,2,3,4,5,6,7,8,9,10,11,12];
  constructor(private router:Router) { }

  ngOnInit() {
  }
  showBooked(){
     this.hide_booked = false;
     this.hide_cancel = true;
     this.hide_toured = true;

  }
  showCancelled(){
     this.hide_booked = true;
     this.hide_cancel  = false;
     this.hide_toured = true;
  }
  showToured(){
     this.hide_booked = true;
     this.hide_cancel  = true;
     this.hide_toured = false;
  }
  

  ngAfterViewInit() {
  	    $(".menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");

      });
      $(document).ready(function() {

          var sidebar = $('#sidebar');
          var sidebarHeader = $('#sidebar .sidebar-header');
          var sidebarImg = sidebarHeader.css('background-image');
          var toggleButtons = $('.sidebar-toggle');

         //Sidebar position
        $('#sidebar-position').change(function() {
            var value = $( this ).val();
            sidebar.removeClass('sidebar-fixed-left sidebar-fixed-right sidebar-stacked').addClass(value).addClass('open');
            if (value == 'sidebar-fixed-left' || value == 'sidebar-fixed-right') {
                $('.sidebar-overlay').addClass('active');
            }
            // Show toggle buttons
            if (value != '') {
                toggleButtons.css('display', 'initial');
                $('body').css('display', 'initial');
            } else {
                // Hide toggle buttons
                toggleButtons.css('display', 'none');
                $('body').css('display', 'table');
            }
        });
      });
      (function($) {
        var dropdown = $('.dropdown');

        // Add slidedown animation to dropdown
        dropdown.on('show.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
        });

        // Add slideup animation to dropdown
        dropdown.on('hide.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
        });
    })(jQuery);  
  }

}
