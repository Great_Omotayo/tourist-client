import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSidebarComponent } from './view-sidebar.component';

describe('ViewSidebarComponent', () => {
  let component: ViewSidebarComponent;
  let fixture: ComponentFixture<ViewSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
