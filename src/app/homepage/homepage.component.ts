import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
location:string = '';
category:string = '';
max_price:string = '0';
  constructor(private router:Router,
  	          private validateService: ValidateService,
  	          private authService: AuthService,
  	          private dataService: DataService) { }

  ngOnInit() {
    alert(window.innerWidth);
  }
  search(){
    let search = {location:this.location,type:this.category,max_price:parseInt(this.max_price)};
    this.dataService.updateSearch(search);
    this.router.navigate(['/search']);
  	// let search = {location:this.location,type:this.category,max_price:parseInt(this.max_price)};
    
  }

}
