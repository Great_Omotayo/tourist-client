import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselForHomepageComponent } from './carousel-for-homepage.component';

describe('CarouselForHomepageComponent', () => {
  let component: CarouselForHomepageComponent;
  let fixture: ComponentFixture<CarouselForHomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselForHomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselForHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
