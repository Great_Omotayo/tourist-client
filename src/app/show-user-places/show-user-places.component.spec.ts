import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUserPlacesComponent } from './show-user-places.component';

describe('ShowUserPlacesComponent', () => {
  let component: ShowUserPlacesComponent;
  let fixture: ComponentFixture<ShowUserPlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowUserPlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUserPlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
