import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
declare var $:any;
@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
status:boolean;
my_account:boolean;
login:boolean;
  constructor(public authService:AuthService) { }

  ngOnInit() {
    this.status = this.authService.loggedIn();
  }
  logOut(){
   this.authService.logOut();
  }
  ngAfterViewInit() {
    $(document).ready(function(){
      $('.showdropdown').click(function(){
         $('.dropdown-container').toggleClass('hide top-for-small');
      });
    });
  }

}
