import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselForXsComponent } from './carousel-for-xs.component';

describe('CarouselForXsComponent', () => {
  let component: CarouselForXsComponent;
  let fixture: ComponentFixture<CarouselForXsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselForXsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselForXsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
