import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { ValidateService } from '../services/validate.service';

declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.css']
})
export class AdminpageComponent implements OnInit {
  hide_booked: boolean = false;
  hide_payment: boolean = true;
  hide_editProfile = true;
  hide_reviews = true;
  hide_status:boolean = true;
  attr:any;
  new_book:number=0;
  transactions:number=0;
  reviews:string;
  visited:number=0;
  wallets:number=0;
  pending_transaction:number=0;
  complete_transaction:number=0;
  likes:string;
  ratings:string;
  booked:any;
  attrname:string;
  attremail:string;
  attrpNumber:string;
  location:string;
  category:string;
  priceperhead:string;
  drivingInstruction:string;
  attraddress:string;
  facilities:any=[];
  facility:string;
  adminName:string;
  adminEmail:string;
  adminpNumber:string;
  hideloading:boolean = true;
  hidemsg:boolean = true;
  state:string;
  msg:string;
  valRes:string;
  added:number = 0;
  bckgnd:string;
  hide_facilities:boolean = true;
  c:number[] = [1,2,3,4,5,6,7,8,9,10,11,12];
  constructor(private dataService: DataService,
              private authService:AuthService,
              private router: Router,
              private validateService: ValidateService) {
               }

  ngOnInit() {
    //this.showBooked();
    this.dataService.currentadmin.subscribe(attr=>{
    if(this.authService.loggedIn() == false)  return this.router.navigate(['/login']);
    if(attr.length ==undefined) {
      this.authService.logOut();
      return this.router.navigate(['/']);
    }
    this.attr = attr;  
    //console.log(this.attr);
    //console.log(this.attr[0].Facilities);
    //this.likes = this.attr[0].Likes == undefined ? 0 : this.attr[0].Likes;
    this.facilities.push.apply(this.facilities,this.attr[0].Facilities);
    });
    this.dataService.currentBooked.subscribe(booked=>{
      this.booked = booked;
      //console.log("am");
      //console.log(this.booked);
      //console.log(this.booked[0].user.email);
    });
  }
  getAttr(){

  }
  showBooked(){
     this.hide_booked = false;
     this.hide_payment = true;
     this.hide_editProfile = true;
     this.hide_reviews = true;
     this.hide_facilities = true;

  }
  editProfile(){
     this.hide_booked = true;
     this.hide_payment = true;
     this.hide_editProfile = false;
     this.hide_reviews = true;
     this.hide_facilities = true;
     this.attrname = this.attr[0].AttractionName;
     this.attremail = this.attr[0].AttractionEmail;
     this.attrpNumber = this.attr[0].AttractionPhone;
     this.location = this.attr[0].AttractionLocation;
     this.category = this.attr[0].AttractionCategory;
     this.priceperhead = this.attr[0].Priceperhead;
     this.drivingInstruction = this.attr[0].drivingInstruction;
     this.attraddress = this.attr[0].AttractionAddress;
     this.adminName = this.attr[0].admin.username;
     this.adminEmail = this.attr[0].admin.email;
     this.adminpNumber = this.attr[0].admin.phone;

  }
  removefacility(facility){
    let index = this.facilities.indexOf(facility);
    this.facilities.splice(index,1);
  }
  add(){
    if(this.facility == '') return;
    this.added = this.added + 1;
    this.facilities.push(this.facility);
    this.facility = '';
    //console.log(this.facilities);
  }
  saveAttraction(){
     let formData  = new FormData;
     formData.append('AttractionName',this.attrname);
     formData.append('AttractionEmail', this.attremail);
     formData.append('AttractionPhone',this.attrpNumber);
     formData.append('AttractionLocation',this.location);
     formData.append('AttractionCategory',this.category);
     formData.append('AttractionAddress',this.attraddress);
     formData.append('Priceperhead',this.priceperhead);
     formData.append('drivingInstruction',this.drivingInstruction);
     formData.append('username',this.adminName);
     formData.append('email',this.adminEmail);
     formData.append('phone',this.adminpNumber);
     this.hideloading = false;
     let val = this.validate();
     this.valRes = this.validateService.editprofile(val);
     if(this.valRes != '') {
       this.hidemsg = false;
       this.state = 'Alert';
       this.bckgnd = 'danger';
       this.msg = this.valRes;
       return;
     }
     this.authService.updateAttr(formData,this.attr._id).subscribe(data=>{
       this.hideloading = true;
       this.hidemsg = false;
       if(data.status == 'success'){
         this.state = 'Success';
         this.bckgnd = 'success';
         this.msg = 'You successfully updated your account';
       }else{
         this.state = 'alert';
         this.bckgnd = 'danger';
         this.msg = "Can't save change, please try again later";
       }
     })
  }
  validate(){
    let data = {
       AttractionName:this.attrname,
       AttractionEmail:this.attremail,
       AttractionPhone:this.attrpNumber,
       AttractionLocation:this.location,
       AttractionCategory:this.category,
       AttractionAddress:this.attraddress,
       Priceperhead:this.priceperhead,
       username:this.adminEmail,
       email:this.adminEmail,
       phone:this.adminpNumber
     }
     return data;
  }
  showPayment(){
     this.hide_booked = true;
     this.hide_payment = false;
     this.hide_editProfile = true;
     this.hide_reviews = true;
     this.hide_facilities = true;

  }
  showReviews(){
     this.hide_booked = true;
     this.hide_payment = true;
     this.hide_editProfile = true;
     this.hide_reviews = false;
     this.hide_facilities = true;
  }
  editFacilities(){
     this.hide_booked = true;
     this.hide_payment = true;
     this.hide_editProfile = true;
     this.hide_reviews = true;
     this.hide_facilities = false;
  }
  savefacilities(){
     if(this.added == 0) return;
     this.authService.updateFacilities(this.facilities,this.attr._id).subscribe(data=>{
       if(data.status =='success'){
         this.state = 'success';
         this.msg = 'You successfully updated your account';
       }else{
         this.state = 'alert'
         this.msg = "Can't save change, please try again later";
       }
     })
     
  }
  status(){
    if(this.hide_status==true){
     this.hide_status = false;
    }
   else{
     this.hide_status = true;
   }
  }
  ngAfterViewInit() {

  $(document).ready(function() {

    var sidebar = $('#sidebar');
    var sidebarHeader = $('#sidebar .sidebar-header');
    var sidebarImg = sidebarHeader.css('background-image');
    var toggleButtons = $('.sidebar-toggle');

    // Hide toggle buttons on default position
    // toggleButtons.css('display', 'none');
    // $('body').css('display', 'table');


    //Sidebar position
    $('#sidebar-position').change(function() {
        var value = $( this ).val();
        sidebar.removeClass('sidebar-fixed-left sidebar-fixed-right sidebar-stacked').addClass(value).addClass('open');
        if (value == 'sidebar-fixed-left' || value == 'sidebar-fixed-right') {
            $('.sidebar-overlay').addClass('active');
        }
        // hide toggle buttons
        if (value != '') {
            toggleButtons.css('display', 'initial');
            $('body').css('display', 'initial');
        } else {
            // Hide toggle buttons
            toggleButtons.css('display', 'none');
            $('body').css('display', 'table');
        }
    });
});
(function($) {
    var dropdown = $('.dropdown');

    // Add slidedown animation to dropdown
    dropdown.on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideup animation to dropdown
    dropdown.on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
})(jQuery);


  }
}
