import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
max_price:number=0;
location:string;
wic:string;
attract:boolean=false;
state:boolean = false;
beach:boolean= false;
reserve:boolean= false;
mountain:boolean=false;
resort:boolean= false;
waterfall:boolean = false;
  constructor(private router:Router,
              private validateService: ValidateService,
              private authService: AuthService,
              private dataService: DataService) { }

  ngOnInit() {
  }
  searchByBudget(){
    let search = {location:'',type:'',max_price:this.max_price};
    this.dataService.updateSearch(search);
    this.router.navigate(['/search']);
  }
  searchByLocation(){
    let search = {location:this.location,type:'',max_price:this.max_price};
    this.dataService.updateSearch(search);
    this.router.navigate(['/search']);
  }
  attraction(type:any){
     if(type=='0' && this.beach == true){
       this.reserve = false;
       this.mountain = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Beach';
     }else if(type == '1' && this.reserve == true){
       this.beach = false;
       this.mountain = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Game Reserve';
     }else if(type == '2' && this.mountain == true){
       this.reserve = false;
       this.beach = false;
       this.resort = false;
       this.waterfall = false;
       this.wic = 'Mountains and Hills';
     }else if(type == '3' && this.resort == true){
       this.reserve = false;
       this.mountain = false;
       this.beach = false;
       this.waterfall = false;
       this.wic = 'Resort';
     }else if(type == '4' && this.waterfall == true){
       this.reserve = false;
       this.mountain = false;
       this.resort = false;
       this.beach = false;
       this.wic = 'Waterfall'
     }
     let search = {location:'',type:this.wic,max_price:this.max_price};
     this.dataService.updateSearch(search);
     this.router.navigate(['/search']);

  }

}
