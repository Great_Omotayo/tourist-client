import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ElementRef,ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
//import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { HttpModule } from '@angular/http'; 
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { TopbarComponent } from './topbar/topbar.component';
import { ContentComponent } from './content/content.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SearchComponent } from './search/search.component';
import { CarouselForHomepageComponent } from './carousel-for-homepage/carousel-for-homepage.component';
import { DealsComponent } from './deals/deals.component';
import { PopularPlacesComponent } from './popular-places/popular-places.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ViewComponent } from './view/view.component';
import { ViewSidebarComponent } from './view-sidebar/view-sidebar.component';
import { CommentOnPlacesComponent } from './comment-on-places/comment-on-places.component';
import {  ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { HeaderCarouselComponent } from './header-carousel/header-carousel.component';
import { TopDestinationComponent } from './top-destination/top-destination.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { BookCentreComponent } from './book-centre/book-centre.component';
import { AttractionComponent } from './attraction/attraction.component';
import { AdminpageComponent } from './adminpage/adminpage.component';
import { AdminReviewsComponent } from './admin-reviews/admin-reviews.component';
import { ViewTabsComponent } from './view-tabs/view-tabs.component';
import { UserPageComponent } from './user-page/user-page.component';
import { ShowUserTopDealsComponent } from './show-user-top-deals/show-user-top-deals.component';
import { ShowUserPlacesComponent } from './show-user-places/show-user-places.component';
import { AdminUploadImagesComponent } from './admin-upload-images/admin-upload-images.component';
import { FooterComponent } from './footer/footer.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ShowUserFrequentlyComponent } from './show-user-frequently/show-user-frequently.component';
import { SidebarForSmallDeviceComponent } from './sidebar-for-small-device/sidebar-for-small-device.component';
import { ViewbComponent } from './viewb/viewb.component';
import { CarouselForXsComponent } from './carousel-for-xs/carousel-for-xs.component';
import { CarouselForSmallDevicesComponent } from './carousel-for-small-devices/carousel-for-small-devices.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { MsgComponent } from './msg/msg.component';
import { DataService } from './services/data.service';
import { ErrmsgComponent } from './services/errmsg/errmsg.component';
import { ErrormsgComponent } from './errormsg/errormsg.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';

@NgModule({
  declarations: [
    AppComponent,
    //FileSelectDirective,
    //FileDropDirective,
    HomepageComponent,
    MainpageComponent,
    TopbarComponent,
    ContentComponent,
    SidebarComponent,
    SearchComponent,
    CarouselForHomepageComponent,
    DealsComponent,
    PopularPlacesComponent,
    LoginComponent,
    RegisterComponent,
    ViewComponent,
    ViewSidebarComponent,
    CommentOnPlacesComponent,
    HeaderCarouselComponent,
    TopDestinationComponent,
    ReviewsComponent,
    BookCentreComponent,
    AttractionComponent,
    AdminpageComponent,
    AdminReviewsComponent,
    ViewTabsComponent,
    UserPageComponent,
    ShowUserTopDealsComponent,
    ShowUserPlacesComponent,
    AdminUploadImagesComponent,
    FooterComponent,
    TermsAndConditionsComponent,
    PrivacyPolicyComponent,
    ShowUserFrequentlyComponent,
    SidebarForSmallDeviceComponent,
    ViewbComponent,
    CarouselForXsComponent,
    CarouselForSmallDevicesComponent,
    SpinnerComponent,
    FileUploadComponent,
    MsgComponent,
    ErrmsgComponent,
    ErrormsgComponent,
    BankDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,  
    HttpModule,
    RouterModule.forRoot([
     {path:'', component:HomepageComponent},
     {path:'content', component:MainpageComponent},
     {path: 'search', component:SearchComponent},
     {path: 'login', component:LoginComponent},
     {path: 'register', component:RegisterComponent},
     {path: 'view', component:ViewComponent},
     {path: 'book', component:BookCentreComponent},
     {path: 'add_attraction', component:AttractionComponent},
     {path: 'adminpage', component:AdminpageComponent},
     {path: 'user', component:UserPageComponent},
     {path: 'terms',component:TermsAndConditionsComponent},
     {path: 'privacy', component:PrivacyPolicyComponent},
     {path: 'msg',component:MsgComponent},
     {path: 'errormsg', component:ErrormsgComponent},
     {path: 'bankdetails', component:BankDetailsComponent},
      ])
  ],
  providers: [ValidateService, AuthService,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
