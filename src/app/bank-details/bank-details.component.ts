import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
det:{tourist:0,amount:0};
tourist:number;
amount:number;
  constructor(private dataService: DataService) { }

  ngOnInit() {
  	this.dataService.currentbankdet.subscribe(det=>{
  		this.det = det;
  		this.tourist = this.det.tourist;
  		this.amount = this.det.amount;
  	});
  }

}
