import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {
  public getattr = new BehaviorSubject<any>({});
  public viewAttr = new BehaviorSubject<any>({});
  public attr_id = new BehaviorSubject<number>(0);
  public towhere = new BehaviorSubject<string>('');
  public toBook  = new BehaviorSubject<any>({});
  public bankdet = new BehaviorSubject<any>({tourist:0,amount:0});
  public admin = new BehaviorSubject<any>({});
  public booked = new BehaviorSubject<any>({});
  public search = new BehaviorSubject<any>({location: "", type: "", max_price: 0});
  currentAttr = this.getattr.asObservable();
  currentview = this.viewAttr.asObservable();
  currentAttr_id = this.attr_id.asObservable();
  currentToWhere = this.towhere.asObservable();
  currenttoBook = this.toBook.asObservable();
  currentbankdet = this.bankdet.asObservable();
  currentadmin = this.admin.asObservable();
  currentBooked = this.booked.asObservable();
  currentsearch = this.search.asObservable();
	 //setStatus(msg:string){this.status.next(msg);}

  constructor() { }
  updateAttr(attr:any){
  	this.getattr.next(attr);
  }
  updateSearch(search:any){
    this.search.next(search);
  }
  viewAttraction(attr:any){
  	this.viewAttr.next(attr);
  }
  updateAttr_id(attr_id:any){
    this.attr_id.next(attr_id);
  }
  updatetoWhere(to:any){
    this.towhere.next(to);
  }
  updatetoBook(book:any){
    this.toBook.next(book);
  }
  updatebankdet(det:any){
    this.bankdet.next(det);
  }
  updateAdmin(attr:any){
    this.admin.next(attr);
  }
  updateBooked(booked:any){
    this.booked.next(booked);
  }
}
