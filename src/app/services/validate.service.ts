import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {
  constructor() { }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  Attraction_details(attr,cpass,image){
  	let msg ='';
    if(attr.AttractionName == undefined || attr.AttractionName ==''){
      return msg += 'Attraction Name field cannot be empty';
    }
    if(attr.AttractionEmail == undefined || attr.AttractionEmail ==''){
       return msg += 'Attraction email field cannot be empty';      
    }
    if(attr.AttractionEmail != ''){
    	const res = this.validateEmail(attr.AttractionEmail);
    	if(res==false)
          return msg += 'Invalid email address';
    }
    if(attr.AttractionPhone == undefined || attr.AttractionPhone ==''){
    	return msg += 'Phone Number is required';
    }
    if(isNaN(attr.AttractionPhone)){
    	return msg += 'Phone Number field: Only number is allowed';
    }
    if(attr.AttractionLocation == undefined){
    	return msg +='Attraction location field is empty';
    }
    if(attr.AttractionCategory == undefined){
    	return msg += 'Category field is empty';
    }
    if(attr.Priceperhead == 0){
      return msg += "Price per head can't be zero";
    }
    if(isNaN(attr.Priceperhead)){
      return msg += 'Price per head field: Only number is allowed'
    }
    //if(attr.Priceperhead == '')
      //return  msg += 'Please specify the price per head';
    if(attr.AttractionAddress == undefined){
    	return msg +='Address is require';
    }
    if(attr.admin.username==undefined){
    	return msg +='Admin name is required';
    }
    if(attr.admin.phone==undefined){
    	return msg +='Admin phone Number is required';
    }
    if(isNaN(attr.admin.phone)){
    	return msg += 'Phone Number field: Only number is allowed'
    }
    if(attr.admin.email == undefined){
    	return msg +='Admin email Address is required';
    }
    if(attr.admin.email != ''){
      const res = this.validateEmail(attr.admin.email);
      if(res==false)
          return msg += 'Invalid email address';
    }
    if(attr.admin.password == undefined || attr.admin.password ==''){
      return msg += 'Password field can not be empty';
    }
    if(attr.admin.password != cpass){
      return msg += 'Password does not matched';
    }
    if(image.length == 0)
      return msg += "Please add at least one image";
    return '';
  }
  editprofile(attr){
    let msg ='';
    if(attr.AttractionName == undefined || attr.AttractionName ==''){
      return msg += 'Attraction Name field cannot be empty';
    }
    if(attr.AttractionEmail == undefined || attr.AttractionEmail ==''){
       return msg += 'Attraction email field cannot be empty';      
    }
    if(attr.AttractionEmail != ''){
      const res = this.validateEmail(attr.AttractionEmail);
      if(res==false)
          return msg += 'Invalid email address';
    }
    if(attr.AttractionPhone == undefined || attr.AttractionPhone ==''){
      return msg += 'Phone Number is required';
    }
    if(isNaN(attr.AttractionPhone)){
      return msg += 'Phone Number field: Only number is allowed';
    }
    if(attr.AttractionLocation == undefined){
      return msg +='Attraction location field is empty';
    }
    if(attr.AttractionCategory == undefined){
      return msg += 'Category field is empty';
    }
    if(attr.Priceperhead == 0){
      return msg += "Price per head can't be zero";
    }
    if(isNaN(attr.Priceperhead)){
      return msg += 'Price per head field: Only number is allowed'
    }
    //if(attr.Priceperhead == '')
      //return  msg += 'Please specify the price per head';
    if(attr.AttractionAddress == undefined){
      return msg +='Address is require';
    }
    if(attr.admin.username==undefined){
      return msg +='Admin name is required';
    }
    if(attr.admin.phone==undefined){
      return msg +='Admin phone Number is required';
    }
    if(isNaN(attr.admin.phone)){
      return msg += 'Phone Number field: Only number is allowed'
    }
    if(attr.admin.email == undefined){
      return msg +='Admin email Address is required';
    }
    if(attr.admin.email != ''){
      const res = this.validateEmail(attr.admin.email);
      if(res==false)
          return msg += 'Invalid email address';
      }
  }
  searchparams(search){
      if(search.location == undefined) search.location = '';
      if(search.type == undefined) search.type = '';
      if(search.max_price == NaN) search.max_price = 0;
      return search;
  }
  validateUserAndBook(book,confirmemail){
    if(book.user.email != confirmemail)
      return "Email doesn't Matched";
    if(book.user.email == '')
      return 'Please supply your email address';
    if(this.validateEmail(book.user.email)==false)
      return 'Invalid email address';
    if(book.tour_date == undefined)
      return 'Please input tour date';
    if(book.tourist == 0 || book.tourist =='')
      return 'Please supply the number of visiting tourist';

      return '';
  }

}
