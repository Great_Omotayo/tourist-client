import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/do";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
     authToken : any;
	 user : any;
	 //url ='/'
	 url ='http://localhost:8080/'
	 attrurl = 'addAttraction';
	 uploadUrl ='upload';
	 loginUrl = 'loggIn';
	 getAttrurl = 'getAttractions';
	 bookurl = 'savebook';
	 updateattr = 'editedProfile'; 
	 updatefac = 'addfacilities';
  constructor(private http: Http, private router:Router) { }
  updateFacilities(facilities,id):Observable<any>{
  	let updatefacilities = this.updatefac+'/'+id;
  	let headers = new Headers({ 'Accept': 'application/json' });
	  	let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.url+''+updatefacilities,facilities,options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  updateAttr(formData,id):Observable<any>{
  	let updateattr = this.updateattr+'/'+id;
  	let headers = new Headers({ 'Accept': 'application/json' });
	  	let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.url+''+updateattr,formData,options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  saveBook(book):Observable<any>{
  	let headers = new Headers({ 'Accept': 'application/json' });
	  	let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.url+''+this.bookurl,book,options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  getAttr(search):Observable<any> {
	  	let headers = new Headers({ 'Accept': 'application/json' });
	  	let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.url+''+this.getAttrurl,search,options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  authenticate(data):Observable<any>{
     let headers = new Headers();
  	headers.append('Accept','application/json');
	let options = new RequestOptions({ headers: headers });
	return this.http.post(this.url+''+this.loginUrl,data, options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  upload(file):Observable<any>{
  	let headers = new Headers();
  	headers.append('Accept','application/json');
	let options = new RequestOptions({ headers: headers });
	return this.http.post(this.url+''+this.attrurl,file, options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
  }
  addAttr(attr): Observable<any> {
	  	let headers = new Headers({ 'Accept': 'application/json' });
	  	let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.url+''+this.attrurl,attr,options)
	                    .map(this.extractData)
	                    .catch(this.handleError);
	                    
   }
   updatelike(like){
   	  localStorage.setItem('Like',like);
   }
   updatecomment(comment){
   	 localStorage.setItem('comment',comment);
   }
   logOut(){
   	 localStorage.clear();
   } 
	private extractData(res: Response) {
	    let data = res.json();
	    return data;
	}
	private handleError (error: Response | any) {
		  // In a real world app, you might use a remote logging infrastructure
		  let errMsg: string;
		  if (error instanceof Response) {
		    const body = error.json() || '';
		    const err = body.error || JSON.stringify(body);
		    errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		  } else {
		    errMsg = error.message ? error.message : error.toString();
		  }
		  console.error(errMsg);
		  return Observable.throw(errMsg);
	}
	savetoLocalStorage(token,attr){
       localStorage.setItem('id_token',token);
       //localStorage.setItem('attr',JSON.stringify(attr));
      }

      loggedIn() {
        return tokenNotExpired('id_token');
      }

}
