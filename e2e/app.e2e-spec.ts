import { TouristClientPage } from './app.po';

describe('tourist-client App', () => {
  let page: TouristClientPage;

  beforeEach(() => {
    page = new TouristClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
